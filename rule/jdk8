# vim:ft=sh:

## install prefix
PREFIX=/usr/jdk

## no process
NO_CONFIGURE=1
NO_BUILD=1

jdk8_update=${VERSION#1.8.}
jdk8_version=8u${jdk8_update}
jdk8_archive_dir=~/Downloads/OracleJDK
jdk8_archive_x64=jdk-${jdk8_version}-solaris-x64.tar.gz
jdk8_install_dir=${DEST_DIR}/${PREFIX}/instances/jdk1.8.0

jdk8_download() {
    local host=http://www.oracle.com
    local url1=/technetwork/java/javase/downloads/index.html
    local url2=$(wget -qO- "${host}${url1}" \
        | perl -ne 'print $& and exit if m{/technetwork/java/javase/downloads/jdk8-downloads-\d+\.html}')
    [[ "${url2}" ]] || return
    wget -qO- "${host}${url2}" \
        | perl -ne 'print $& and exit if m{http://download.oracle.com/otn-pub/java/jdk/'"${jdk8_version}"'-b\d+/'"${jdk8_archive_x64//./\\.}"'}'
}

jdk8_unarchiver() {
    gtar -C "${jdk8_install_dir}" --strip-component=1 -xvf "$1"
}

## fetch sources
_fetch() {
    local jdk8url_x64=$(jdk8_download)
    [[ "${jdk8url_x64}" ]] || return 1
    local cookie='oraclelicense=accept-securebackup-cookie' 
    wget -P "${jdk8_archive_dir}" --header="Cookie: ${cookie}" "${jdk8url_x64}"
}

## install phase
_install() {
    mkdir -p "${jdk8_install_dir}"
    jdk8_unarchiver "${jdk8_archive_dir}/${jdk8_archive_x64}"
    ln -s instances/jdk1.8.0 "${DEST_DIR}/${PREFIX}/jdk1.8.0_$(printf "%02d" "${jdk8_update}")"
}
