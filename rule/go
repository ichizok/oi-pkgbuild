# vim:ft=sh:

work_goroot=${WORK_ROOT}/go
work_gopath=${HOME}/.golang

PREFIX_BASE=${PREFIX}
PREFIX=${PREFIX}/go

_fetch() {
    local version=$(<<<"${VERSION}" cut -d. -f1-2)
    local branch=release-branch.go${version}
    cd "${work_goroot}"
    git status --short | awk '$1=="??"{print$2}' | xargs rm -f
    git reset --hard
    git checkout master
    git pull --ff --ff-only
    if git branch -r | grep -q "origin/${branch}"; then
        if git branch | grep -qs "${branch}"; then
            git checkout "${branch}"
            git merge --ff --ff-only "origin/${branch}"
        else
            git checkout -b "${branch}" "origin/${branch}"
        fi
    fi
}

NO_CONFIGURE=1

bootstrap_go() {
    if type go &>/dev/null; then
        export GOROOT_BOOTSTRAP=$(go env GOROOT) 
        return
    fi
    # TODO build go1.4
    #export GOROOT_BOOTSTRAP=${go14}
}

build_go() {
    GOROOT_FINAL=${PREFIX} GOOS=solaris GOARCH=amd64 GOHOSTARCH=amd64 CGO_ENABLED=1 ./make.bash
}

build_godoc() {
    (
    local godoc_src=golang.org/x/tools/cmd/godoc
    PATH=${work_goroot}/bin:$PATH
    mkdir -p "${work_gopath}"
    GOROOT=${work_goroot} GOPATH=${work_gopath} go get -u -v "${godoc_src}"
    GOROOT=${work_goroot} GOPATH=${work_gopath} go install "${godoc_src}"
    )
}

_build() {
    cd "${work_goroot}/src"
    bootstrap_go
    build_go
    build_godoc
}

_install() {
    mkdir -p "${INST_DIR}"
    tar -cC "${work_goroot}" \
        $(find "${work_goroot}" -maxdepth 1 -mindepth 1 -type d ! -name '.*' | sed 's,^.*/,,') \
        | tar -xC "${INST_DIR}"
}

_post_install() {
    local bindir=${DEST_DIR}${PREFIX_BASE}/bin
    mkdir -p "${bindir}/amd64"
    ln -s amd64 "${bindir}/64"
    for exe in "${INST_DIR}/bin"/*; do
        ln -s ../../go/bin/"${exe##*/}" "${bindir}/amd64"
    done
}

_generate_payload() {
    _generate_payload_default
    sed -i '/\.a$/s/mode=0644/mode=0666/' "${PKG_PAYLOAD}"
}
