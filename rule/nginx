# vim:ft=sh:

_fetch() {
    wget -P "${WORK_ROOT}" "http://nginx.org/download/nginx-${VERSION}.tar.gz"
}

CONFIGURE=./configure
CONFIGURE_ENV="CC=${CC}"
CONFIGURE_ARG="\
    --prefix=/var/nginx \
    --user=webservd \
    --group=webservd \
    --with-cc-opt='-Wno-error -O2 -I/usr/local/include' \
    --with-ld-opt='-L/usr/local/lib/amd64 -Wl,-rpath=/usr/local/lib/amd64' \
    --with-pcre \
    --with-pcre-jit \
    --conf-path=/etc/nginx/nginx.conf \
    --error-log-path=/var/nginx/logs/error.log \
    --http-client-body-temp-path=/var/tmp/nginx/client_body \
    --http-fastcgi-temp-path=/var/tmp/nginx/fastcgi \
    --http-log-path=/var/nginx/logs/access.log \
    --http-proxy-temp-path=/var/tmp/nginx/proxy \
    --http-scgi-temp-path=/var/tmp/nginx/scgi \
    --http-uwsgi-temp-path=/var/tmp/nginx/uwsgi \
    --lock-path=/var/run/nginx/lock \
    --pid-path=/var/run/nginx/pid \
    --sbin-path=/usr/local/nginx/sbin/amd64/nginx \
    --with-http_addition_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_gzip_static_module \
    --with-http_random_index_module \
    --with-http_realip_module \
    --with-http_secure_link_module \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_sub_module \
    --with-ipv6 \
    --with-select_module \
    --with-http_v2_module \
    --with-http_gunzip_module \
    "

_post_install() {
    tar -cC "${DEST_DIR}.data" lib var | tar -xC "${DEST_DIR}"
}

_generate_payload() {
    _generate_payload_default
    _merge_payload_files "${PKG_PAYLOAD}" "${PKG_PAYLOAD}.data"
    sed -i -e '/path=\(lib\/svc\|var\/\(run\|tmp\)\)$/d' \
        -e '/^dir .* path=lib\/svc\/manifest/s/ group=[^ ]*/ group=sys/' "${PKG_PAYLOAD}"
}
