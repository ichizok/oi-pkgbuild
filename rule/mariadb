# vim:ft=sh:

MARIADB_SERIES=$(<<<${VERSION} cut -d. -f1-2)

MARIADB_LIBRARY_PROTO="${PKG_PROTO}%2Flibrary"
MARIADB_TESTS_PROTO="${PKG_PROTO}%2Ftests"

PREFIX=/usr/local/mariadb/${MARIADB_SERIES}
PREFIX_NV="${PREFIX%/*}"

PATCHES=(mariadb.patch)

BUILD_DIR="${PKG_DIR}/MYSQLBUILD"


_fetch() {
    wget -P "${WORK_ROOT}" "http://ftp.osuosl.org/pub/mariadb/mariadb-${VERSION}/source/mariadb-${VERSION}.tar.gz"
}

_clean_target() {
    CLAEN_TARGET=(
    "${PKG_PROTO}"/*.old
    "${PKG_PROTO}"/*.old.*
    "${MARIADB_LIBRARY_PROTO}"/*.old 
    "${MARIADB_LIBRARY_PROTO}"/*.old.*
    "${MARIADB_TESTS_PROTO}"/*.old
    "${MARIADB_TESTS_PROTO}"/*.old.*
    )
}

_pre_configure() {
    mkdir -p "${BUILD_DIR}" && cd "${BUILD_DIR}"
}

CONFIGURE='cmake ..'
CONFIGURE_ARG="-DCMAKE_C_COMPILER=${CC} -DCMAKE_CXX_COMPILER=${CXX} \
    -DCMAKE_INSTALL_PREFIX=${PREFIX} -DSYSCONFDIR=/etc/mariadb/${MARIADB_SERIES} \
    -DINSTALL_BINDIR=bin/amd64 -DINSTALL_SBINDIR=sbin/amd64 \
    -DINSTALL_LIBDIR=lib/amd64 -DINSTALL_MYSQLSHAREDIR=share/mariadb \
    -DMYSQL_DATADIR=/var/mariadb/${MARIADB_SERIES}/data -DWITH_SSL=yes -DWITH_PIC=yes -DDEFAULT_CHARSET=utf8 \
    -DDEFAULT_COLLATION=utf8_general_ci -DMYSQL_UNIX_ADDR=/var/tmp/mariadb.sock \
    -DENABLE_DTRACE=OFF"

_pre_build() {
    local links="${PKG_RULE}.links"
    [[ -f "$links" ]] || return 1
    _info "===> Check whether appropriate compiler is specified..."
    while read link
    do
        if [[ -f "$link" ]] && grep -qs "${CC}" "$link"; then
            _info "======> Fix $link"
            sed -i "s/${CC//./\\.}/${CXX}/" "$link"
        fi
    done <"$links"
}

MARIADB_LIBRARY="${MARIADB_LIBRARY_PROTO}/ROOT"
MARIADB_TESTS="${MARIADB_TESTS_PROTO}/ROOT"

install_mariadb() {
    find "${INST_DIR}" -maxdepth 1 -type f -print0 | xargs -0 rm
    mv "${INST_DIR}/scripts/"* "${INST_DIR}/bin"
    mv "${INST_DIR}/support-files/"* "${INST_DIR}/share/mariadb"
    rmdir "${INST_DIR}/scripts" "${INST_DIR}/support-files"
    ln -s amd64 "${INST_DIR}/bin/64"
    ln -s amd64 "${INST_DIR}/sbin/64"
    for d in bin data docs include man share
    do
        ln -s ${MARIADB_SERIES}/$d "${DEST_DIR}${PREFIX_NV}/$d"
    done
}

install_mariadb_library() {
    mkdir -p "${MARIADB_LIBRARY}${PREFIX}"
    mv "${INST_DIR}/lib" "${MARIADB_LIBRARY}${PREFIX}"
    find "${MARIADB_LIBRARY}${PREFIX}" -name '*.a' -print0 | xargs -0 rm
    ln -s ${MARIADB_SERIES}/lib "${MARIADB_LIBRARY}${PREFIX_NV}/lib"
    ln -s amd64 "${MARIADB_LIBRARY}${PREFIX}/lib/64"
}

install_mariadb_tests() {
    mkdir -p "${MARIADB_TESTS}${PREFIX}"
    mv "${INST_DIR}/mysql-test" "${MARIADB_TESTS}${PREFIX}"
    mv "${INST_DIR}/sql-bench" "${MARIADB_TESTS}${PREFIX}"
    ln -s ${MARIADB_SERIES}/mysql-test "${MARIADB_TESTS}${PREFIX_NV}/mysql-test"
    ln -s ${MARIADB_SERIES}/sql-bench "${MARIADB_TESTS}${PREFIX_NV}/sql-bench"
}

_post_install() {
    _info "### mariadb/library"
    _prepare_dist "${MARIADB_LIBRARY%/*}"
    install_mariadb_library

    _info "### mariadb/tests"
    _prepare_dist "${MARIADB_TESTS%/*}"
    install_mariadb_tests

    _info "### mariadb"
    install_mariadb
}

_generate_payload() {
    _info "### mariadb/library"
    _generate_payload_default "${PKG_PROTO}%2Flibrary"

    _info "### mariadb/tests"
    _generate_payload_default "${PKG_PROTO}%2Ftests"

    _info "### mariadb"
    _generate_payload_default

    sed -e "s/__MARIADB_SERIES__/${MARIADB_SERIES}/g" "${PKG_PAYLOAD}.data" >>"${PKG_PAYLOAD}"
    _merge_payload_files "${PKG_PAYLOAD}"

    tar -cC "${DEST_DIR}.data" etc lib usr var | tar -xC "${DEST_DIR}"
    find "${DEST_DIR}.data" -type f | while read; do
        sed -i -e "s/__MARIADB_SERIES__/${MARIADB_SERIES}/g" "${DEST_DIR}/${REPLY#${DEST_DIR}.data/}"
    done
}

_generate_manifest() {
    _info "### mariadb/library"
    _generate_manifest_default "${PKG_PROTO}%2Flibrary"

    _info "### mariadb/tests"
    _generate_manifest_default "${PKG_PROTO}%2Ftests"

    _info "### mariadb"
    _generate_manifest_default
}

_publish() {
    _info "### mariadb/library"
    _publish_default "${PKG_PROTO}%2Flibrary"

    #_info "### mariadb/tests"
    #_publish_default "${PKG_PROTO}%2Ftests"

    _info "### mariadb"
    _publish_default
}
