#!/bin/bash

. "${0%/*}"/environ.sh

declare -r USAGE_MSG=`cat <<EOT
Usage:
    $(_command) pkg-name version
EOT
`
[[ $# -lt 2 ]] && _usage

declare -r PKG_NAME=$1
declare -r VERSION=$2

declare -r OS_BRANCH=$(cat "${BRANCH_FILE}" 2>/dev/null)
if [[ $? -ne 0 ]] || [[ ! "${OS_BRANCH}" ]]; then
    _assert "Couldn't fetch OS_BRANCH.\nYou should execute 'pkgbuild update -b <os-branch>'."
fi

declare -r PKGPUB_DIR=${PKGPUB_DIR:-/opt/pub/oidev}
declare -r PKG_PROTO=${PROTO_INST}/${PKG_NAME}
declare -r PKG_PAYLOAD=${PKG_PROTO}/payload
declare -r PKG_MANIFEST=${PKG_PROTO}/manifest
cd "${PKG_PROTO}"

declare -r DEST_DIR=${PKG_PROTO}/ROOT
declare -r PKG_RULE=${RULE_DIR}/${PKG_NAME}
_load_pkg_rule

declare -r INST_DIR=${DEST_DIR}${PREFIX}

_merge_payload_files() {
    local -A lines
    local key arg ord
    local payload=$1
    local tmpfile=$(mktemp)
    for arg in "$@"; do
        while read; do
            key=$(grep -o '\bpath=[^ ]*' <<<"${REPLY}")
            case "${REPLY}" in
                file*)
                    ord=0;;
                dir*)
                    ord=1;;
                *)
                    ord=2;;
            esac
            lines["${key}"]=${ord}${REPLY}
        done <"${arg}"
    done
    for arg in "${lines[@]}"; do
        echo "${arg}"
    done | sort | sed 's/^.//' >"${tmpfile}"
    mv "${tmpfile}" "${payload}"
}

_generate_payload_default() {
    local pkg_proto=${1:-${PKG_PROTO}}
    local destdir=${pkg_proto}/ROOT
    local payload=${pkg_proto}/payload
    pkgsend generate "${destdir}" | gsed '/\<path=[^\/]\+\s*$/d' >"${payload}"
}

_generate_manifest_default() {
    local pkg_proto=${1:-${PKG_PROTO}}
    local info=${pkg_proto}/info
    local payload=${pkg_proto}/payload
    local manifest=${pkg_proto}/manifest
    sed -e "s/__VERSION__/${VERSION}/g" \
        -e "s/__OS_BRANCH__/${OS_BRANCH}/g" "${info}" >"${manifest}"
    cat "${payload}" >>"${manifest}"
}

_publish_default() {
    local pkg_proto=${1:-${PKG_PROTO}}
    ( cd "${pkg_proto}" \
        && pfexec pkgsend -s "${PKGPUB_DIR}" publish -d ROOT --fmri-in-manifest manifest )
}

_save_version() {
    local verfile=${PKG_PROTO}/version 
    echo "${VERSION}" >"${verfile}"
    (
    cd "${PROTO_INST}"
    if [[ "$(git status --short "${verfile}" 2>/dev/null)" ]]; then
        git commit "${verfile}" -m"${PKG_NAME}: ${VERSION}"
    fi
    )
}

if [[ ! "${NO_PAYLOAD}" ]]; then
    _info "===> Generate payload..."
    _call_if_defined _generate_payload || _assert "Failed."

    [[ "${PAYLOAD_ONLY}" ]] && exit 0
fi

if [[ ! "${NO_MANIFEST}" ]]; then
    _info "===> Generate manifest..."
    _call_if_defined _generate_manifest || _assert "Failed."

    [[ "${MANIFEST_ONLY}" ]] && exit 0
fi

_info "===> Publish to '${PKGPUB_DIR}'..."
_call_if_defined _publish || _assert "Failed."

_info "===> Save package version..."
_save_version
