#!/bin/bash

. "${0%/*}"/environ.sh

declare -r USAGE_MSG=`cat <<EOT
Usage:
    $(_command) [-d pkg-dir] [-w] pkg-name version

Options:
    -d pkg-dir  Specify package directory
    -w          Overwrite payload, manifest, ROOT (no backup)
EOT
`
[[ $# -lt 2 ]] && _usage

declare -r PKG_NAME=$1
declare -r VERSION=$2
shift 2

while getopts 'd:w' OPT; do
    case $OPT in
        d) PKG_DIR=$OPTARG
            ;;
        w) OVERWRITE=y
            ;;
    esac
done

_backup() {
    local bak=$1
    local ext=${2:-old}
    if [[ -e "$bak" ]]; then
        rm -fr "$bak.$ext"
        mv "$bak" "$bak.$ext"
    fi
}

_prepare_dist() {
    local destdir=$1/ROOT
    local payload=$1/payload
    local manifest=$1/manifest
    local bakext=old.$(date -d"@$(stat -c'%Y' "${manifest}")" '+%Y%m%dT%H%M%SZ')
    if [[ "${OVERWRITE}" ]]; then
        rm -fr "${destdir}"
    else
        _backup "${destdir}" "${bakext}"
        _backup "${payload}" "${bakext}"
        _backup "${manifest}" "${bakext}"
    fi
    mkdir -p "${destdir}"
}

_apply_patches_default() {
    local patchfile=
    if [[ "${PATCHES}" ]]; then
        for patchfile in ${PATCHES[@]}; do
            patch -p1 <"${RULE_DIR}/${patchfile}" || return 1
        done
    fi
}

_runcmd() {
    _debug "$@"; eval "$@"
}

_configure_default() {
    _runcmd ${CONFIGURE_ENV} ${CONFIGURE} ${CONFIGURE_ARG}
}

_build_default() {
    _runcmd ${MAKE} ${MAKE_ARG-'-j8'} ${BUILD_OPT}
}

_install_default() {
    _runcmd ${MAKE} DESTDIR="'${DEST_DIR}'" ${INSTALL_OPT} install
}

_symlink64() {
    local basedir=$1
    local linkdir=
    for linkdir in bin sbin lib libexec
    do
        linkdir=${basedir}/${linkdir}
        if [[ -d "${linkdir}/amd64" ]] && [[ ! -e "${linkdir}/64" ]]; then
            ln -s amd64 "${linkdir}/64"
        fi
    done
}

_remove_staticlib() {
    find "${INST_DIR}/lib" -regex '.*/lib.*\.\(a\|la\)' -print0 | xargs -0 rm -f
}

declare -r PKG_DIR=${PKG_DIR:-$PWD}
[[ ! -d "${PKG_DIR}" ]] && _assert "${PKG_DIR}: No such directory."

cd "${PKG_DIR}"

#if type clang.64 &>/dev/null; then
#    declare -x CC=${CC:-clang.64}
#    declare -x CXX=${CXX:-clang++.64}
#else
    declare -x CC=${CC:-gcc.64}
    declare -x CXX=${CXX:-g++.64}
#fi

declare -x PREFIX=/usr/local

declare -r PKG_PROTO=${PROTO_INST}/${PKG_NAME}
declare -r DEST_DIR=${PKG_PROTO}/ROOT

declare -r PKG_RULE=${RULE_DIR}/${PKG_NAME}
_load_pkg_rule || _assert "${PKG_NAME}: No such file"

declare -r INST_DIR=${DEST_DIR}${PREFIX}

declare -r CONFIGURE=${CONFIGURE:-configure.64 ./configure}
declare -r MAKE=${MAKE:-make}
declare -r INSTALL=${INSTALL:-ginstall -c}

_info "===> Applying patches..."
_call_if_defined _apply_patches || _assert "Failed to apply patches."

if [[ ! "${NO_CONFIGURE}" ]]; then
    _info "===> Configuration..."

    _call_if_defined _pre_configure || _assert "pre-configure failed."

    _call_if_defined _configure || _assert "Failed to configure."

    _call_if_defined _post_configure || _assert "post-configure failed."

    [[ "${CONFIGURE_ONLY}" ]] && exit 0
fi


if [[ ! "${NO_BUILD}" ]]; then
    _info "===> Build software..."

    _call_if_defined _pre_build || _assert "pre-build failed."

    _call_if_defined _build || _assert "Failed to build."

    _call_if_defined _post_build || _assert "post-build failed."

    [[ "${BUILD_ONLY}" ]] && exit 0
fi


if [[ ! "${NO_INSTALL}" ]]; then
    _info "===> Installation..."

    _call_if_defined _pre_install || _assert "pre-install failed."

    _prepare_dist "${PKG_PROTO}"

    _call_if_defined _install || _assert "Failed to install."

    _call_if_defined _post_install || _assert "post-install failed."
fi

_info "===> Make symlink (directories of amd64 to 64)..."
_symlink64 "${INST_DIR}"

_info "===> Finish."
