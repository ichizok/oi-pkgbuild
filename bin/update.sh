#!/bin/bash

. "${0%/*}"/environ.sh

declare -r USAGE_MSG=`cat <<EOT
Usage:
    $(_command) -b os-branch
    $(_command) [-u] [-R dir] [pkg-name]

Options:
    -b          Specify branch signiture
    -u          Upload manifest
    -R          Local repository

Example:
    # Set branch to 2014.0.0.0
    $(_command) -b 2014.0.0.0

    # Upload manifest, assume yes
    $(_command) -u -R /opt/pub/oidev vim
EOT
`

while OPTERR=0 getopts 'b:uR:' OPT; do
    case $OPT in
        b) OS_BRANCH=$OPTARG
            ;;
        u) UPLOAD=y
            ;;
        R) PKGPUB_DIR=$OPTARG
            ;;
        *) _usage
            ;;
    esac
done
shift $((OPTIND - 1))

if [[ "${OS_BRANCH}" ]]; then
    echo "${OS_BRANCH}" >"${BRANCH_FILE}"
else
    OS_BRANCH=$(cat "${BRANCH_FILE}" 2>/dev/null)
fi

declare -r PKG_NAME=$1

if [[ "${PKGPUB_DIR}" ]]; then
    export PKGPUB_DIR
fi

_get_version() {
    local version=$1/version
    cat "${version}" 2>/dev/null
}

_upload() {
    local pkgname=${1##*/}
    local version=$(_get_version "$1")
    if [[ "${version}" ]]; then
        "${0%/*}"/publish.sh "${pkgname}" "${version}"
    fi
}

if [[ "$UPLOAD" ]]; then
    if [[ ! "${OS_BRANCH}" ]]; then
        _assert "Couldn't fetch OS_BRANCH.\nYou should execute '$0 -b <os-branch>'."
    fi

    cd "${PROTO_INST}"

    if [[ "${PKG_NAME}" ]]; then
        if [[ ! -d "${PKG_NAME}" ]]; then
            _assert "${PKG_NAME}: No such package."
        fi
        _upload "${PKG_NAME}"
    else
        for p in *; do
            _upload "$p"
        done
    fi
fi
