#!/bin/bash

. "${0%/*}"/environ.sh

declare -r USAGE_MSG=`cat <<EOT
Usage:
    $(_command) [-fx] pkg-name version

Options:
    -f          Download file even if already exists
    -x          Extract archive
EOT
`

while getopts 'fx' OPT; do
    case $OPT in
        f) FORCE_DL=y
            ;;
        y) EXTRACT=y
            ;;
    esac
done
shift $((OPTIND - 1))

[[ $# -lt 2 ]] && _usage

_check_version() {
    [[ "$1" =~ ^([0-9]+\.)+[0-9]+$ ]]
}

_fetch_default() {
    _warn "Not implemented"
}

_extract_default() {
    _warn "Not implemented"
}

declare -r PKG_NAME=$1
declare -r VERSION=$2

declare -r PKG_RULE=${RULE_DIR}/${PKG_NAME}
_load_pkg_rule

_info "===> Downloading..."
_call_if_defined _fetch || _assert "Failed to download"

if [[ "${EXTRACT}" ]]; then
    _info "===> Extract..."
    _call_if_defined _extract || _assert "Failed to extract"
fi
