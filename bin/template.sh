#!/bin/bash

. "${0%/*}"/environ.sh

declare -r USAGE_MSG=`cat <<EOT
Usage:
    $(_command) pkg-name
EOT
`

[[ $# -lt 1 ]] && _usage

declare -r PKG_NAME=$1
declare -r PKG_RULE=${RULE_DIR}/${PKG_NAME}

if [[ -f "${PKG_RULE}" ]]; then
    read -p "File exists. Overwrite it? [y/N]: "
    [[ $REPLY =~ ^[yY]([eE][sS])?$ ]] || exit
fi

cat >"${PKG_RULE}" <<'EOT'
# vim:ft=sh:

## install prefix
#PREFIX=/usr/local

## patches
#PATCHES=()

## fetch sources
#_fetch() {
#}

## apply patches
#_apply_patches() {
#}

## pre-configure phase
#_pre_configure() {
#}

## configure phase
#NO_CONFIGURE=
#CONFIGURE_ONLY=

#CONFIGURE_ENV=
#CONFIGURE_ARG=

#_configure() {
#}

## pre-build phase
#_pre_build() {
#}

## build phase
#NO_BUILD=
#BUILD_ONLY=

#MAKE_FLAG=
#BUILD_OPT=

#_build() {
#}

## pre-install phase
#_pre_install() {
#}

## install phase
#NO_INSTALL=

#INSTALL_OPT=

#_install() {
#}

## post-install phase
#_post_install() {
#}

## publishing phase
#NO_PAYLOAD=
#_generate_manifest() {
#}

#NO_MANIFEST=
#_generate_manifest() {
#}

#_publish() {
#}
EOT
