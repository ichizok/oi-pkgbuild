set -e
shopt -s nullglob

declare -r RULE_DIR=~/work/BUILD/rule
declare -r WORK_ROOT=~/work
declare -r PROTO_INST=~/work/proto_inst
declare -r BRANCH_FILE=${PROTO_INST}/.branch

declare -A color
if [[ -t 1 ]]; then
    color=(
        ['gray']="\e[1;30m"
        ['red']="\e[1;31m"
        ['green']="\e[1;32m"
        ['yellow']="\e[1;33m"
        ['blue']="\e[1;34m"
        ['magenta']="\e[1;35m"
        ['cyan']="\e[1;36m"
        ['white']="\e[1;37m"
        ['reset']="\e[m"
        )
fi

_command() {
    local name=${0##*/}
    echo "pkgbuild ${name%.sh}"
}

_debug() {
    if [[ $VERBOSE =~ ^[1yY] ]]; then
        echo "${color['yellow']}DEBUG: $@${color['reset']}" >&2
    fi
}

_info() {
    echo "${color['green']}$@${color['reset']}"
}

_warn() {
    echo "${color['yellow']}$@${color['reset']}"
}

_error() {
    echo "${color['red']}$@${color['reset']}"
}

_assert() {
    _error "$@"
    exit 1
}

_usage() {
    echo "$@" "$USAGE_MSG\n" >&2
    exit 1
}

_load_pkg_rule() {
    [[ -f "${PKG_RULE}" ]] && . "${PKG_RULE}"
}

_is_defined() {
    type "$1" &>/dev/null
}

_call_if_defined() {
    local fn=$1
    shift
    if _is_defined "$fn"; then
        "$fn" "$@"
    elif _is_defined "$fn"_default; then
        "$fn"_default "$@"
    fi
}
