#!/bin/bash

. "${0%/*}"/environ.sh

declare -r USAGE_MSG=`cat <<EOT
Usage:
    $(_command) [-y] pkg-name

Options:
    -y          Clean without confirmation
EOT
`

while getopts 'y' OPT; do
    case $OPT in
        y) ASSUME_YES=y
            ;;
    esac
done
shift $((OPTIND - 1))

[[ $# -lt 1 ]] && _usage

declare -r PKG_NAME=${1//\//%2F}
declare -r PKG_PROTO=${PROTO_INST}/${PKG_NAME}

declare -r PKG_RULE=${RULE_DIR}/${PKG_NAME}
_load_pkg_rule

declare -a CLAEN_TARGET

_clean_target_default() {
    CLAEN_TARGET=("${PKG_PROTO}"/*.old "${PKG_PROTO}"/*.old.*)
}

_call_if_defined _clean_target

if [[ ${#CLAEN_TARGET[@]} -eq 0 ]]; then
    _assert "No file or directory to be removed."
fi

_info "The following files/dirs will be removed."
for tgt in "${CLAEN_TARGET[@]}"; do
    dir=${tgt%/*}
    echo " ${dir##*/}/${tgt##*/}"
done

if [[ ! "${ASSUME_YES}" ]]; then
    read -p "Do you want to remove these? [Y/n]: "
else
    REPLY=${ASSUME_YES}
fi

if [[ ! $REPLY =~ ^[nN] ]]; then
    rm -fr "${CLAEN_TARGET[@]}"
fi
